																		<strong>Projet chef d'oeuvre:</strong>



<strong>Présentation</strong>

Afin de terminer l'année et de présenter un projet solide lors du passage de l'examen, nous devons réaliser un projet chef d'oeuvre. 

Afin d'identifier un besoin, j'ai utilisé mes expériences professionnelles précédentes.

J'ai ainsi choisi de créer une plateforme communautaire qui permet l'entraide et le partage de connaissances et compétences au sein d'une structure, quelle qu'elle soit (association, entreprise, écoles...).

Souvent, quand on est nouveau au sein d'une entreprise, les collègues nous forment sur différents outils utilisés en interne. Cette période de formation ne dure que quelques jours, au mieux une semaine. Ensuite, le nouvel arrivant est lancé dans l'arène. 

Malheureusement, certaines personnes n'osent pas demander de l'aide sur des incompréhensions ou sur des difficultés à utiliser des outils. Elles restent sur des blocages et des difficultés de compréhension.

La plateforme que je vais vous présenter vient aider ces personnes et le bon fonctionnement de la communauté. Je propose d'étendre ces périodes de formation et de les créer en fonction du besoin des usagerères et usagers. 

Concrètement, il s'agit d'une plateforme où d'une part un/e utilisateur/trice va demander une session de formation sur un outil particulier. D'autre part, un/e utilisateur/trice propose son aide sur cet outil. 

Les utilisateurs/trices auront aussi la possibilité de partager des ressources et autres documents afin de partager leurs connaissances. 

S'offre alors la possibilité aux usagers/ères d'accèder à un agenda commun où ils ont la possibilité de bloquer un créneau. 

<strong>Maquettes fonctionnelles </strong>

* maquettes mobile first : 
* maquettes tablette :
* maquettes desktop: 



<strong>User Story </strong>

1) En tant qu'usagère/usager de la plateforme, je souhaite me créer un compte afin de pouvoir faire des demandes à mes collègues.

2) En tant que membre d'une communauté (quelle qu'elle soit), je souhaite demander de l'aide sur cette plateforme afin de dépasser une situation de blocage.

3) En tant que membre d'une communauté, je souhaite avoir accès à toutes les demandes d'aide afin d'être alerté des besoins de mes collègues.

4) En tant que membre d'une communauté, je souhaite avoir accès à toutes les demandes d'aide afin de voir si je peux aider une personne en partageant des ressources ou des documents. 

5) En tant que membre d'une communauté, je souhaite "matcher" avec une demande d'aide afin de permettre à une personne de débloquer une situation.

6) En tant que membres d'une communauté ayant trouvés leur paire "demandeur d'aide/aidant", nous souhaitons avoir accès à l'agenda partagé afin de convenir d'un moment.

7) En tant que membre d'une communauté, je souhaite avoir accès à toutes les demandes d'aide afin de me greffer à un groupe d'aide déjà formé.



<strong>Organisation du travail</strong>





